class Cell:
    
    """ Constructor for a Cell """
    def __init__(self, x_position, y_position):
        self.x_position = x_position
        self.y_position = y_position
        self.shot = False
        self.ship = None
        self.hit = None
	
#	# MT: Commented this out, not sure if necessary or used
#    """ Defines a shot on a cell """
#    def shot(self):
#        assert(self.shot == False)
#        assert(self.hit == False)
#        self.shot = True
#        if self.ship != None:
#            self.hit = True
#        else:
#            self.hit = False
    
    """ Assigns a ship to a cell """
    def set_ship(self, ship):
        assert((self.ship == None) or (ship == None))
        self.ship = ship
    
    """ Get x_position """
    def get_x_position(self):
        return self.x_position
    
    """ Get y_position """
    def get_y_position(self):
        return self.y_position
    
    """ Get ship """
    def get_ship(self):
        return self.ship
    
    """ Get shot information """
    def get_shot_status(self):
        return self.shot
    
    """ Get hit status """
    def get_hit_status(self):
        return self.hit
    
    """ Set shot status on aux grid"""
    def set_shot(self, truth):
        self.shot = truth

    def get_shot(self):
        return self.shot

