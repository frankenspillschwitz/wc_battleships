import numpy as np
import math
import matplotlib
from matplotlib import pyplot as plt
from matplotlib import patches
import string

class Plot:
    def __init__(self, size):
        self.fig = plt.figure()
        self.size = size+1
        self.p1 = fig.add_subplots(1,2,1)
        plt.xticks(range(self.size))
        plt.yticks(range(self.size))
        plt.rc('grid', linestyle="-", color='black')
        plt.grid(True)
        self.p2 = fig.add_subplots(1,2,2)
        plt.xticks(range(self.size))
        plt.yticks(range(self.size))
        plt.rc('grid', linestyle="-", color='black')
        plt.grid(True)
        #plt.show()

    def add_Patch(self,x,y,col):
        rect=patches.Rectangle((1, 1),x,y,col)    
        self.add_patch(rect)
	
    def get_subplot(self,player_name):
        if player_name == "Player 1":
            return self.p1
        else:
            return self.p2

    def redraw(self):
        self.fig.canvas.draw();
