class Ship:
    
    # Constructor
    def __init__(self, name, start_pos, size, orientation):
        self.name = name;
        self.start_pos = start_pos;
        self.size = size;
        self.orientation = orientation;
        self.hits = 0;
        self.sunk = False;
        self.damaged = False;
    
    # Method for get name
    def get_name(self):
        return self.name
	
	# Method for get start_pos
    def get_start_pos(self):
        return self.start_pos
	
	# Method for get size
    def get_size(self):
        return self.size
	
	# Method for get orientation
    def get_orientation(self):
        return self.orientation
	
	# Method for get hits
    def get_hits(self):
        return self.hits
	
	# Method for get sunk
    def get_sunk(self):
        return self.sunk
	
	# Method for get damaged
    def get_damaged(self):
        return self.damaged
    
    # Method for set name
    def set_name(self, name):
        self.name = name;
	
	# Method for set start_pos
    def set_start_pos(self, start_pos):
        self.start_pos = start_pos;
		
	# Method for set size
    def set_size(self, size):
        self.size = size;
		
	# Method for set orientation
    def set_orientation(self, orientation):
        self.orientation = orientation;
		
	# Method for set hits
    def set_hits(self, hits):
    	self.hits = hits;
    
	# Method for set sunk
    def set_sunk(self):
        assert(self.sunk == False)
        self.sunk = True;
		
	# Method for set damaged
    def set_damaged(self):
        assert(self.sunk == False)
        self.damaged = True;
		
	# Method for hits
    def inc_hits(self):
    	self.hits = self.hits+1;


