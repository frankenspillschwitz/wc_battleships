import unittest
from battleships import Cell
from battleships import Ship

class CellTests(unittest.TestCase):
	
	def test_creation(self):
		sc = Cell(2,4)
		
	def test_val_property(self):
		sc = Cell(3,4)
		
		self.assertEqual(sc.x_position,3)
		self.assertEqual(sc.y_position,4)
		self.assertEqual(sc.shot,False)
		self.assertEqual(sc.ship,None)
		self.assertEqual(sc.hit,None)
		
	def test_getter(self):
		sc = Cell(5,6)
		xpos = sc.get_x_position()
		ypos = sc.get_y_position()
		ship = sc.get_ship()
		shot = sc.get_shot_status()
		hit = sc.get_hit_status()
		shot2 = sc.get_shot()
		
		self.assertEqual(xpos,sc.x_position)
		self.assertEqual(ypos,sc.y_position)
		self.assertEqual(ship,sc.ship)
		self.assertEqual(shot,sc.shot)
		self.assertEqual(hit,sc.hit)
		self.assertEqual(shot2,sc.shot)
		
	def test_set_ship(self):
		sc = Cell(2,3)
		
		self.assertIsNone(sc.ship)
		sc.set_ship(Ship('Carrier', [2,3], 2, 'H'))
		self.assertIsNotNone(sc.ship)
		self.assertIsInstance(sc.ship,Ship)
		try:
			sc.set_ship(Ship('Submarine', [2,3], 3, 'H'))
			self.assertTrue(False)
		except:
			pass
		self.assertNotEqual(sc.ship,Ship('Submarine', [2,3], 3, 'H'))
		
	def test_set_shot(self):
		sc = Cell(1,3)
		
		sc.set_shot(False)
		self.assertFalse(sc.shot)
		
		sc.set_shot(True)
		self.assertTrue(sc.shot)
		
	# Not sure if we need the shot() method in Cell?
	# Also, I think if it is needed then shot() needs to be renamed because it is a property and a method of the same name?
#	def test_shot(self):
#		sc = Cell(2,4)
#		
#		sc.shot()
#		self.assertEqual(sc.shot, True)
#		self.assertEqual(sc.hit, False)
#		
#		sc.set_ship(True)
#		sc.shot()
#		self.assertEqual(sc.shot, True)
#		self.assertEqual(sc.hit, True)


