import unittest
from battleships import Grid
from battleships import Cell

class GridTests(unittest.TestCase):
	
	def test_creation(self):
		sc = Grid(5)
		
	def test_val_property(self):
		
		# Is this where mocking is needed?
		# Or do we test that grid is an instance of cell?
		sc = Grid(10)
		self.assertIsInstance(sc,Grid)
		
		# sc.cell_array is a list
		self.assertIsInstance(sc.cell_array,list)
		
	def test_getter(self):
		sc = Grid(5)
		gridarray = sc.get_cell_array()
		
		self.assertEqual(gridarray,sc.cell_array)
		
		# Cell(2,3) is an instance of Cell
		self.assertIsInstance(sc.get_cell(2,3),Cell)
		self.assertEqual(sc.get_cell(2,3),gridarray[1][2])
		
