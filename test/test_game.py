import unittest
from battleships import Game
from battleships import Side

class GameTests(unittest.TestCase):
	
	def test_creation(self):
		sc = Game('testcases/pos_fleet1.txt','testcases/pos_place1.txt','testcases/pos_place1.txt')
		
	def test_val_property(self):
		sc = Game('testcases/pos_fleet1.txt','testcases/pos_place1.txt','testcases/pos_place1.txt')
		
		# Fleet field/property
		self.assertIsInstance(sc.fleet,dict)
		
		# Side field/property x2
		self.assertIsInstance(sc.side1,Side)
		self.assertIsInstance(sc.side2,Side)
		
	def test_read_fleet(self):
		# Implement
		pass
		
	def test_read_csv(self):
		# Implement
		pass
		
	def test_read_layout(self):
		# Implement
		pass
		
	def test_move(self):
		# Implement
		pass
		
	def test_run_moves(self):
		# Implement
		pass
	
	
