from battleships import Game

def str_pn(c):
    if c == 0:
        return 'neg_'
    else:
        return 'pos_'

def char_pn(c):
    if c == 0:
        return '-'
    else:
        return '+'

def com_pn(w,c,i):
    if c == 0:
        return ' ' + w + '-' + str(i)
    else:
        return ' ' + w + '+' + str(i)

def test(fc, fi, p1c, p1i, p2c, p2i, mc, mi):
    d = 'testcases/'
    a = '.txt'
    fn = d + str_pn(fc) + 'fleet' + str(fi) + a
    p1n = d + str_pn(p1c) + 'place' + str(p1i) + a
    p2n = d + str_pn(p2c) + 'place' + str(p2i) + a
    mn = d + str_pn(mc) + 'move' + str(mi) + a
    c = fc * p1c * p2c * mc
    try:
        print(fn)
        print(p1n)
        print(p2n)
        game = Game(fn, p1n, p2n)
        print('some')
        game.run_moves(mn)
        print('yes')
        return c
    except Exception:
        print('no')
        return 1 - c

s = 0
f = 0
ep = []
en = []
for fc in range(0,2):
    for fi in range(1,4-2*fc):
        for p1c in range(0,2):
            for p1i in range(1,3-p1c):
                for p2c in range(0,2):
                    for p2i in range(1,3-p2c):
                        for mc in range(0,2):
                            for mi in range(1,3):
                                c = fc * p1c * p2c * mc
                                d =com_pn('f',fc,fi)+com_pn('p1',p1c,p1i)+com_pn('p2',p2c,p2i)+com_pn('m',mc,mi)
                                r = test(fc, fi, p1c, p1i, p2c, p2i, mc, mi)
                                if r == 0:
                                    f += 1
                                    m = ''
                                    if c == 0:
                                        m = 'success'
                                        en += [d]
                                    else:
                                        m = 'failure'
                                        ep += [d]
                                    print('Unexpected ' + m + ': ' + d)
                                else:
                                    s += 1
                                    print(d)
print('False negatives')
for d in ep:
    print(d)
print('False positives')
for d in en:
    print(d)
print(str(s) + ' successes and ' + str(f) + ' failures.')

